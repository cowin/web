# CoWin Web

Alternative CoWin website that makes it easier to book vaccination appointments.

I made this website because I felt the original website made by the Indian Government - [cowin.gov.in](https://www.cowin.gov.in/) lacked some features that can make the whole booking process easier. So I thought I'll make my own web app with [React](https://reactjs.org/) with a UX upgrade.

## The Upgrade

### Save button

In Kerala, the state where I live, I had troubles finding vaccine slots that are free and available for my age group. So I had to check the original Cowin website frequently to see if any new slots were available. This meant that I had to input my state, district and age group every time I visited the website.

So for my website, I made a "Save" button inside the side drawer, below all the inputs. It saves the inputs I entered. So the next time I open my website, the state, district and age group are all already entered and the results are also already loaded.

<img width="840" src="screenshots/cowin0.png" />

### Notifications for new slots

Saving the inputs also enables notifications on my website. In the future, when new slots are available based on the criteria i entered, the website will send me a notificatoin. This saves me the trouble of having to check the website all the time.

### Sorting based on availability

The original website shows the session in an order that is inconvenient. Sometimes it shows the fully booked sessions on top and you have to scroll down to the bottom to check if there is any available sessions.

<img width="840" src="screenshots/cowin1.png" />

So I designed this app such that it shows the available sessions first.

<img width="840" src="screenshots/cowin2.png" />

### Better UI

This is a minor upgrade. I designed the table that shows the sessions based on the UI used by [Google Calendar](https://calendar.google.com/calendar/u/0). I think that design is simpler and more convenient.

### Dark Mode

If you are like me and use dark mode in most of your apps, I think you would appreciate this feature.

<img width="840" src="screenshots/cowin3.png" />

## Technology Stack

- I used [React](https://reactjs.org/) to create this single page application.
- The UI components are from the [Material-UI](http://material-ui.com/) library.
- I have used [Redux](https://redux.js.org/) for state management.
- The application is deployed in [Firebase](https://firebase.google.com/). It also uses the Cloud Messaging and Cloud Functions feature of Firebase to enable notifications.
- Building and deploying tasks are automated using [GitLab CI/CD](https://docs.gitlab.com/ee/ci/).

## Contact Me

If you have any suggestions on how I can improve this app, please do let me know. The currently only displays the availability. You have to login to the original website to make the bookings. I'm working on integrating the booking part as well.

Please leave your suggestion by creating an [issue](https://gitlab.com/cowin/web/-/issues) in this project or mail me at [jagannathbhatjb@gmail.com](mailto:jagannathbhatjb@gmail.com).
