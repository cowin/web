export const SET_DARK_MODE = 'SET_DARK_MODE'

export const LOADING_HIDE = 'LOADING_HIDE'
export const LOADING_SHOW = 'LOADING_SHOW'
