import { SET_DARK_MODE } from './types'

const initialState = true

const darkModeReducer = (state = initialState, action) => {
	switch (action.type) {
		case SET_DARK_MODE:
			return action.payload
		default:
			return state
	}
}

export default darkModeReducer
