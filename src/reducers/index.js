import { combineReducers } from 'redux'

import darkModeReducer from './darkModeReducer'
import loadingModalReducer from './loadingModalReducer'

export default combineReducers({
	darkMode: darkModeReducer,
	loadingModal: loadingModalReducer,
})
