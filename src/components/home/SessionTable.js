import React, { Fragment } from 'react'
import {
	Button,
	Chip,
	makeStyles,
	Table,
	TableBody,
	TableCell,
	TableContainer,
	TableHead,
	TableRow,
	Typography,
} from '@material-ui/core'

import { getDayString, getDate } from '../../actions/dateActions'
import { useStyles } from '../../theme'
import { Menu as MenuIcon } from '@material-ui/icons'

const SessionsTable = ({ headers, rows }) => {
	const classes = useStyles()
	const localClasses = useLocalStyles()

	const Body = () => {
		if (rows.length === 0) return <RowNoSession />
		else {
			return (
				<>
					{rows.map(row => (
						<TableRow key={row.name}>
							<CellHospital {...row} />
							{row.cells.map((sessions, index) => (
								<CellSessions key={index} sessions={sessions} />
							))}
						</TableRow>
					))}
				</>
			)
		}
	}

	const CellHospital = ({ address, fee_type, name, pincode }) => (
		<TableCell className={localClasses.hospitalCell} component='th' scope='row'>
			<Typography variant='h6'>
				{name}{' '}
				{fee_type !== 'Free' && (
					<Chip color='primary' label={fee_type} size='small' />
				)}
			</Typography>
			<Typography component='p' variant='subtitle2'>
				{address}, {pincode}
			</Typography>
		</TableCell>
	)

	const CellSessions = ({ sessions }) => (
		<TableCell align='center'>
			{sessions.map(session => (
				<Fragment key={session.session_id}>
					{session.show && (
						<div className={localClasses.session}>
							<Typography color='primary' component='span'>
								{session.vaccine}
							</Typography>
							<br />
							<Typography component='span' variant='caption'>
								{session.age_limit === 2
									? '18-44'
									: `${session.min_age_limit}+`}
							</Typography>
							<br />
							{session.available_capacity > 0 ? (
								<>
									{session.available_capacity_dose1 > 0 && (
										<a
											className={`${classes.linkNoStyle} ${localClasses.bookLink}`}
											href='https://www.cowin.gov.in'
											rel='noreferrer'
											target='_blank'
										>
											<Button
												color='secondary'
												size='small'
												variant='contained'
											>
												Dose 1 ({session.available_capacity_dose1})
											</Button>
										</a>
									)}
									{session.available_capacity_dose2 > 0 && (
										<a
											className={`${classes.linkNoStyle} ${localClasses.bookLink}`}
											href='https://www.cowin.gov.in'
											rel='noreferrer'
											target='_blank'
										>
											<Button
												color='secondary'
												size='small'
												variant='contained'
											>
												Dose 2 ({session.available_capacity_dose2})
											</Button>
										</a>
									)}
								</>
							) : (
								<Button disabled size='small' variant='contained'>
									Booked
								</Button>
							)}
						</div>
					)}
				</Fragment>
			))}
		</TableCell>
	)

	const HeaderDay = ({ header }) => (
		<TableCell align='center'>
			<Typography
				color='textSecondary'
				component='p'
				gutterBottom
				variant='body1'
			>
				{getDayString(header)}
			</Typography>
			<Typography color='textSecondary' component='p' variant='h5'>
				{getDate(header)}
			</Typography>
		</TableCell>
	)

	const RowNoSession = () => (
		<TableRow>
			<TableCell align='center' colSpan={8}>
				<Typography vairant='h3'>No Sessions Available</Typography>
			</TableCell>
		</TableRow>
	)

	if (headers.length > 0)
		return (
			<TableContainer style={{ maxHeight: 'calc(100vh - 64px)' }}>
				<Table
					aria-label='Appointments table'
					stickyHeader
					style={{ minWidth: 600 }}
				>
					<TableHead>
						<TableRow>
							<TableCell>
								<Typography color='textSecondary' variant='h6'>
									Hospital
								</Typography>
							</TableCell>
							{headers.map(header => (
								<HeaderDay key={header} header={header} />
							))}
						</TableRow>
					</TableHead>
					<TableBody>
						<Body />
					</TableBody>
				</Table>
			</TableContainer>
		)
	else
		return (
			<Typography className={localClasses.noInputMessage} variant='h5'>
				Please press the <MenuIcon /> icon (top left) and select location.
			</Typography>
		)
}

const useLocalStyles = makeStyles(theme => ({
	bookLink: {
		display: 'inline-block',
		margin: theme.spacing(1),
	},
	hospitalCell: {
		maxWidth: 240,
		minWidth: 140,
		verticalAlign: 'top',
		wordBreak: 'break-word',
	},
	noInputMessage: {
		margin: theme.spacing(4),
	},
	session: {
		marginBottom: theme.spacing(2),
	},
}))

export default SessionsTable
