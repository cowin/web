import React, { useEffect, useState } from 'react'
import axios from 'axios'
import localforage from 'localforage'
import {
	AppBar,
	Button,
	Drawer,
	FormControl,
	FormControlLabel,
	IconButton,
	TextField,
	InputLabel,
	makeStyles,
	MenuItem,
	Select,
	Switch,
	Toolbar,
	Typography,
} from '@material-ui/core'
import {
	Brightness4 as Brightness4Icon,
	Menu as MenuIcon,
} from '@material-ui/icons'
import { ChevronLeft, ChevronRight } from '@material-ui/icons'
import { connect } from 'react-redux'

import SessionsTable from '../home/SessionTable'
import { setDarkMode } from '../../actions/darkModeActions'
import {
	loadingModalHide,
	loadingModalShow,
} from '../../actions/loadingModalActions'
import {
	getFullDateString,
	getMonthString,
	getWeek,
} from '../../actions/dateActions'
import { useStyles } from '../../theme'

const API_FIND_BY_DISTRICT =
	'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id='
const API_FIND_BY_PINCODE =
	'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode='
const API_GET_DISTRICTS =
	'https://cdn-api.co-vin.in/api/v2/admin/location/districts'
const API_GET_STATES = 'https://cdn-api.co-vin.in/api/v2/admin/location/states'

const calcAgeLimit = data => {
	return data.map(center => {
		center.sessions = center.sessions.map(session => {
			const age_limit =
				session.min_age_limit === 45 ? 3 : session.max_age_limit ? 2 : 1
			return { ...session, age_limit }
		})
		return center
	})
}

const enableNotification = async () => {
	try {
		if (Notification.permission !== 'granted')
			await Notification.requestPermission()
		// inform user
	} catch (e) {
		console.log(e.message)
	}
}

const filterApply = (data, filters, inputs) =>
	data.reduce((centers, center) => {
		if (!inputs.paid && center.fee_type === 'Paid') return centers
		const newSessions = center.sessions.filter(item => {
			if (!inputs.booked && item.available_capacity === 0) return false
			for (const index in filters) {
				if (filters[index] && item[index] !== filters[index]) return false
			}
			return true
		})
		return newSessions.length > 0
			? [...centers, { ...center, sessions: newSessions }]
			: centers
	}, [])

const filterApplySessions = (centers, filters, paid) =>
	centers.reduce((sessions, center) => {
		if (!paid && center.fee_type === 'Paid') return sessions
		const newSessions = center.sessions.filter(item => {
			if (item.available_capacity === 0) return false
			for (const index in filters) {
				if (filters[index] && item[index] !== filters[index]) return false
			}
			return true
		})
		return newSessions.length > 0 ? [...sessions, ...newSessions] : sessions
	}, [])

const formatData = (data, week) => {
	const headerIndexMap = week.map(header => getFullDateString(header))

	data = data.map(center => {
		const cells = [[], [], [], [], [], [], []]

		for (let session of center.sessions) {
			const index = headerIndexMap.indexOf(session.date)

			if (cells[index]) cells[index].push({ ...session, show: true })
			else cells[index] = [{ ...session, show: true }]
		}

		return { ...center, cells }
	})

	return data
}

const sortCenters = data => {
	data = data.map(center => {
		let max_available_capacity = 0
		let age_limit = 0

		for (const session of center.sessions) {
			if (max_available_capacity < session.available_capacity)
				max_available_capacity = session.available_capacity
			if (age_limit > session.age_limit) age_limit = session.age_limit
		}
		return { ...center, age_limit, max_available_capacity }
	})

	data.sort((a, b) => {
		if (a.age_limit === b.age_limit)
			return b.max_available_capacity - a.max_available_capacity
		return a.age_limit - b.age_limit
	})

	return data
}

const mapStateToProps = state => ({ darkMode: state.darkMode })

const HomePage = ({
	darkMode,
	loadingModalHide,
	loadingModalShow,
	setDarkMode,
}) => {
	const defaultFilters = [
		{ index: 'age_limit', title: '18+', value: 1 },
		{ index: 'age_limit', title: '18-44', value: 2 },
		{ index: 'age_limit', title: '45+', value: 3 },
		{ index: 'vaccine', value: 'COVISHIELD', title: 'COVISHIELD' },
	]
	const initialApi = { day: null, param: null, url: null }
	const initialInputs = {
		booked: false,
		district: 0,
		state: 0,
		paid: true,
		pincode: '',
	}
	const initialFiltersApplied = { age_limit: null, vaccine: null }

	const [api, setApi] = useState(initialApi)
	const [data, setData] = useState([])
	const [districts, setDistricts] = useState([])
	const [drawerShow, setDrawerShow] = useState(false)
	const [inputs, setInputs] = useState(initialInputs)
	const [filters, setFilters] = useState(defaultFilters)
	const [filtersApplied, setFiltersApplied] = useState(initialFiltersApplied)
	const [tableHeaders, setTableHeaders] = useState([])
	const [tableRows, setTableRows] = useState([])
	const [states, setStates] = useState([])

	const classes = useStyles()
	const localClasses = useLocalStyles()

	const changeHandlerDistrict = ({ target: { value } }) => {
		setInputs({ ...inputs, district: value })
		setApi({ day: new Date(), param: value, url: API_FIND_BY_DISTRICT })
	}

	const changeHandlerPincode = ({ target: { value } }) => {
		setInputs({ ...initialInputs, pincode: value })
		if (value.length > 5)
			setApi({ day: new Date(), param: value, url: API_FIND_BY_PINCODE })
	}

	const changeHandlerState = ({ target: { value } }) => {
		setInputs({ ...initialInputs, state: value })
		setTableHeaders([])
		setTableRows([])

		loadingModalShow('HomePageGetDistricts')
		axios
			.get(`${API_GET_DISTRICTS}/${value}`)
			.then(res => setDistricts(res.data.districts))
			.finally(loadingModalHide('HomePageGetDistricts'))
	}

	const changeHandlerSwitch = ({ target: { name } }) => {
		const copyData = [...data]
		const newValue = !inputs[name]

		const newInputs = { ...inputs, [name]: newValue }

		setInputs(newInputs)
		setTableRows(
			formatData(
				sortCenters(filterApply(copyData, filtersApplied, newInputs)),
				tableHeaders
			)
		)
	}

	const changeWeekCurrent = () =>
		setApi({ day: new Date(), param: inputs.district, url: api.url })

	const changeWeekNext = () => {
		const day = new Date(tableHeaders[6])
		day.setDate(day.getDate() + 1)
		setApi({ day, param: inputs.district, url: api.url })
	}

	const changeWeekPrevious = () => {
		const day = new Date(tableHeaders[0])
		day.setDate(day.getDate() - 7)
		if (day < getWeek()[0]) return null
		setApi({ day, param: inputs.district, url: api.url })
	}

	const compileCenters = (resData, week) => {
		resData = calcAgeLimit(resData)

		setData(resData)
		setTableHeaders(week)
		setFilters(findFilterParameters(resData))

		setTableRows(
			formatData(
				sortCenters(filterApply(resData, filtersApplied, inputs)),
				week
			)
		)
	}

	const filterRows = (index, value) => {
		const newData = [...data]
		const newFilters = {
			...filtersApplied,
			[index]: filtersApplied[index] === value ? null : value,
		}

		setFiltersApplied(newFilters)
		setTableRows(
			formatData(
				sortCenters(filterApply(newData, newFilters, inputs)),
				tableHeaders
			)
		)
	}

	const findFilterParameters = data => {
		const filterNames = filters.map(filter => `${filter.index}${filter.value}`)

		for (const center of data)
			for (let session of center.sessions) {
				if (!filterNames.includes(`age_limit${session.age_limit}`)) {
					filterNames.push(`age_limit${session.age_limit}`)

					filters.push({
						index: 'age_limit',
						title: session.max_age_limit
							? `18-44`
							: `${session.min_age_limit}+`,
						value: session.age_limit,
					})
				}
				if (!filterNames.includes(`vaccine${session.vaccine}`)) {
					filterNames.push(`vaccine${session.vaccine}`)
					filters.push({
						index: 'vaccine',
						value: session.vaccine,
						title: session.vaccine,
					})
				}
			}

		filters.sort((a, b) => (a.title < b.title ? -1 : 1))

		return filters
	}

	const saveInputs = async () => {
		await localforage.setItem('districts', JSON.stringify(districts))
		await localforage.setItem('filtersApplied', JSON.stringify(filtersApplied))
		await localforage.setItem('inputs', JSON.stringify(inputs))
		await localforage.setItem('states', JSON.stringify(states))
		await localforage.setItem(
			'sessions',
			JSON.stringify(
				filterApplySessions([...data], { ...filtersApplied }, inputs.paid)
			)
		)
		enableNotification()
		setDrawerShow(false)
	}

	useEffect(() => {
		loadingModalShow('LoadingConfiguration')
		localforage
			.getItem('darkmode')
			.then(value => setDarkMode(value === 'true'))
			.then(() => localforage.getItem('districts'))
			.then(value => {
				if (value) setDistricts(JSON.parse(value))
				else setDrawerShow(true)
			})
			.then(() => localforage.getItem('filtersApplied'))
			.then(value => {
				if (value) setFiltersApplied(JSON.parse(value))
			})
			.then(() => localforage.getItem('states'))
			.then(value => {
				if (!value || JSON.parse(value).length === 0)
					axios.get(API_GET_STATES).then(res => setStates(res.data.states))
				else setStates(JSON.parse(value))
			})
			.then(() => localforage.getItem('inputs'))
			.then(value => {
				if (!value) return

				value = JSON.parse(value)
				setInputs({ ...inputs, ...value })

				if (value.district && value.district !== 0)
					setApi({
						day: new Date(),
						param: value.district,
						url: API_FIND_BY_DISTRICT,
					})
				else if (value.pincode && value.pincode !== '')
					setApi({
						day: new Date(),
						param: value.pincode,
						url: API_FIND_BY_PINCODE,
					})
			})
			.finally(loadingModalHide('LoadingConfiguration'))
		// eslint-disable-next-line
	}, [])

	useEffect(() => {
		if (api.url) {
			loadingModalShow('HomePageGetData')
			axios
				.get(`${api.url}${api.param}&date=${getFullDateString(api.day)}`)
				.then(res => compileCenters(res.data.centers, getWeek(api.day)))
				.finally(loadingModalHide('HomePageGetData'))
		}
		// eslint-disable-next-line
	}, [api])

	return (
		<>
			<AppBar position='static'>
				<Toolbar>
					<IconButton
						aria-label='menu'
						color='inherit'
						edge='start'
						onClick={() => setDrawerShow(true)}
					>
						<MenuIcon />
					</IconButton>
					<Typography variant='h6'>CowinJB</Typography>
					<div className={localClasses.header}>
						{tableHeaders.length > 0 && (
							<>
								<Button
									className={localClasses.thisWeek}
									onClick={changeWeekCurrent}
									size='small'
									variant='outlined'
								>
									This Week
								</Button>
								<div className={localClasses.dateController}>
									<Typography component='span' variant='h6'>
										<IconButton
											aria-label='Previous Week'
											color='inherit'
											onClick={changeWeekPrevious}
										>
											<ChevronLeft />
										</IconButton>{' '}
										<IconButton
											aria-label='Next Week'
											color='inherit'
											onClick={changeWeekNext}
										>
											<ChevronRight />
										</IconButton>
										{getMonthString(tableHeaders[0], tableHeaders[6])}{' '}
									</Typography>
								</div>
							</>
						)}
					</div>
				</Toolbar>
			</AppBar>
			<div className={localClasses.root}>
				<SessionsTable headers={tableHeaders} rows={tableRows} />
			</div>
			<Drawer open={drawerShow} onClose={() => setDrawerShow(false)}>
				<div className={localClasses.drawerHeader}>
					<IconButton
						aria-label='close'
						color='inherit'
						onClick={() => setDrawerShow(false)}
					>
						<MenuIcon />
					</IconButton>
				</div>
				<div className={localClasses.drawerMain}>
					<FormControl className={localClasses.input} variant='outlined'>
						<InputLabel id='state-label'>State</InputLabel>
						<Select
							autoFocus
							id='state'
							label='State'
							labelId='state-label'
							name='state'
							onChange={changeHandlerState}
							value={inputs.state}
							variant='outlined'
						>
							<MenuItem value={0}>Select</MenuItem>
							{states.map(state => (
								<MenuItem key={state.state_id} value={state.state_id}>
									{state.state_name}
								</MenuItem>
							))}
						</Select>
					</FormControl>
					<FormControl className={localClasses.input} variant='outlined'>
						<InputLabel id='district-label'>District</InputLabel>
						<Select
							disabled={inputs.state === 0}
							id='district'
							label='District'
							labelId='district-label'
							name='district'
							onChange={changeHandlerDistrict}
							value={inputs.district}
						>
							<MenuItem value={0}>Select</MenuItem>
							{districts.map(district => (
								<MenuItem
									key={district.district_id}
									value={district.district_id}
								>
									{district.district_name}
								</MenuItem>
							))}
						</Select>
					</FormControl>
					<Typography style={{ textAlign: 'center' }}>OR</Typography>
					<FormControl className={localClasses.input}>
						<TextField
							id='pincode'
							label='Pincode'
							name='pincode'
							onChange={changeHandlerPincode}
							type='number'
							value={inputs.pincode}
							variant='outlined'
						/>
					</FormControl>
					<div className={localClasses.switches}>
						<FormControlLabel
							control={
								<Switch
									checked={inputs.booked}
									onChange={changeHandlerSwitch}
									name='booked'
									color='primary'
								/>
							}
							label='Show Booked'
						/>
						<FormControlLabel
							control={
								<Switch
									checked={inputs.paid}
									onChange={changeHandlerSwitch}
									name='paid'
									color='primary'
								/>
							}
							label='Show Paid'
						/>
					</div>
					<div className={localClasses.filters}>
						<Typography vairant='h5' gutterBottom>
							Filters
						</Typography>
						{filters.map(filter => (
							<Button
								color={
									filtersApplied[filter.index] === filter.value
										? 'primary'
										: 'default'
								}
								key={filter.title}
								onClick={() => filterRows(filter.index, filter.value)}
								style={{ margin: '6px' }}
								variant='outlined'
							>
								{filter.title}
							</Button>
						))}
					</div>
					<div className={localClasses.save}>
						<Button
							className={localClasses.input}
							onClick={saveInputs}
							variant='contained'
						>
							Save
						</Button>
						<br />
						<Typography variant='caption'>
							Click here to save your location and filters and{' '}
							<b>get notified when new slots appear</b>.
						</Typography>
					</div>
				</div>{' '}
				<div className={localClasses.drawerFooter}>
					<a
						className={classes.linkNoStyle}
						href='https://gitlab.com/cowin/web'
						rel='noreferrer'
						target='_blank'
					>
						<Typography
							className={localClasses.link}
							variant='body1'
							component='span'
						>
							GitLab repository
						</Typography>
					</a>
					<IconButton
						aria-label='dark mode'
						color='inherit'
						onClick={() => setDarkMode(!darkMode)}
					>
						<Brightness4Icon />
					</IconButton>
				</div>
			</Drawer>
		</>
	)
}

const useLocalStyles = makeStyles(theme => ({
	dateController: {
		display: 'inline-block',
		margin: theme.spacing(2),
		verticalAlign: 'middle',
	},
	drawerFooter: {
		alignItems: 'center',
		display: 'flex',
		justifyContent: 'space-between',
		padding: theme.spacing(4),
	},
	drawerMain: {
		display: 'flex',
		flex: 1,
		flexDirection: 'column',
		maxWidth: '360px',
		padding: theme.spacing(4),
	},
	filters: {
		margin: theme.spacing(2),
		marginTop: theme.spacing(4),
	},
	header: {
		textAlign: 'right',
	},
	input: {
		margin: theme.spacing(2),
		minWidth: 120,
	},
	root: {
		flex: 1,
		overflow: 'hidden',
	},
	save: {
		marginTop: theme.spacing(4),
		textAlign: 'center',
	},
	switches: {
		margin: `${theme.spacing(3)}px ${theme.spacing(2)}px`,
	},
	thisWeek: {
		borderColor: 'inherit',
		color: 'inherit',
		marginLeft: theme.spacing(6),
	},
}))

export default connect(mapStateToProps, {
	loadingModalHide,
	loadingModalShow,
	setDarkMode,
})(HomePage)
