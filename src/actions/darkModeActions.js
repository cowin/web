import localforage from 'localforage'

import { SET_DARK_MODE } from '../reducers/types'

export const setDarkMode = mode => async dispatch => {
	try {
		await localforage.setItem('darkmode', `${mode}`)
		dispatch({ payload: mode, type: SET_DARK_MODE })
	} catch (err) {
		console.error(err)
	}
}
