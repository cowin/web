export const doubleDigit = num => (num < 10 ? '0' + num : num)

export const getFullDateString = (date = new Date()) =>
	`${doubleDigit(date.getDate())}-${doubleDigit(
		date.getMonth() + 1
	)}-${date.getFullYear()}`

export const getDate = (date = new Date()) => doubleDigit(date.getDate())

export const getDayString = (date = new Date()) => {
	switch (date.getDay()) {
		case 0:
			return 'Sun'
		case 1:
			return 'Mon'
		case 2:
			return 'Tue'
		case 3:
			return 'Wed'
		case 4:
			return 'Thu'
		case 5:
			return 'Fri'
		case 6:
			return 'Sat'
		default:
			return ''
	}
}

export const getMonthString = (date = new Date(), date2 = new Date()) => {
	const showNextMonth = date.getDate() > date2.getDate()
	switch (date.getMonth()) {
		case 0:
			return showNextMonth ? 'Jan/Feb' : 'Jan'
		case 1:
			return showNextMonth ? 'Feb/Mar' : 'Feb'
		case 2:
			return showNextMonth ? 'Mar/Apr' : 'Mar'
		case 3:
			return showNextMonth ? 'Apr/May' : 'Apr'
		case 4:
			return showNextMonth ? 'May/Jun' : 'May'
		case 5:
			return showNextMonth ? 'Jun/Jul' : 'Jun'
		case 6:
			return showNextMonth ? 'Jul/Aug' : 'Jul'
		case 7:
			return showNextMonth ? 'Aug/Sep' : 'Aug'
		case 8:
			return showNextMonth ? 'Sep/Oct' : 'Sep'
		case 9:
			return showNextMonth ? 'Oct/Nov' : 'Oct'
		case 10:
			return showNextMonth ? 'Nov/Dec' : 'Nov'
		case 11:
			return showNextMonth ? 'Dec/Jan' : 'Dec'
		default:
			return ''
	}
}

export const getWeek = (today = new Date()) => {
	const week = []

	for (let index = 0; index < 7; index++) {
		week.push(
			new Date(today.getFullYear(), today.getMonth(), today.getDate() + index)
		)
	}

	return week
}
