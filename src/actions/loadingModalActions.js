import { LOADING_HIDE, LOADING_SHOW } from '../reducers/types'

export const loadingModalHide = id => async dispatch => {
	try {
		dispatch({ payload: id, type: LOADING_HIDE })
	} catch (err) {
		console.error(err)
	}
}

export const loadingModalShow = id => async dispatch => {
	try {
		dispatch({ payload: id, type: LOADING_SHOW })
	} catch (err) {
		console.error(err)
	}
}
