import React, { useEffect } from 'react'
import { Provider } from 'react-redux'
import { initializeApp } from 'firebase/app'
import { getMessaging, getToken, onMessage } from 'firebase/messaging'

import './App.css'
import store from './store'
import Page from './Page'

const firebaseConfig = {
	apiKey: 'AIzaSyBenxZ9GXopf1pmnTHNGcqPtcAFM3XgP6s',
	authDomain: 'cowinjb.firebaseapp.com',
	projectId: 'cowinjb',
	storageBucket: 'cowinjb.appspot.com',
	messagingSenderId: '233103940496',
	appId: '1:233103940496:web:1585fa6fc01da5b2b67115',
	measurementId: 'G-R7XGL799QF',
}

const App = () => {
	initializeApp(firebaseConfig)
	const messaging = getMessaging()

	useEffect(() => {
		getToken(messaging, {
			vapidKey:
				'BN0DAT3RTFDLER471M_lDGshzwIUvXL7Lo5f2Jp4IcfFagNRJScyx0CCK4ZgKho80A1T1Qkvrt6VvZu6eYohg4M',
		})
			.then(currentToken => {
				if (currentToken) {
					const config = {
						body: currentToken,
						method: 'POST',
					}
					fetch('https://us-central1-cowinjb.cloudfunctions.net/subscribe', config)
						.then(res => res.json())
						.then(data => console.log(data))
						.catch(err => console.error(err))
				} else {
					console.log(
						'No registration token available. Request permission to generate one.'
					)
				}
			})
			.catch(err => {
				console.log('An error occurred while retrieving token. ', err)
			})

		onMessage(messaging, ({ data: { title, body } }) => {
			if (title === 'checkSessions') window.location.reload()
			else console.log('Message from server', title, body)
		})
		// eslint-disable-next-line
	}, [])

	return (
		<Provider store={store}>
			<Page />
		</Provider>
	)
}

export default App
