import React from 'react'
import {
	createMuiTheme,
	CssBaseline,
	makeStyles,
	ThemeProvider,
	Typography,
	withWidth,
} from '@material-ui/core'
import { blue } from '@material-ui/core/colors'
import { connect } from 'react-redux'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'

import HomePage from './components/pages/HomePage'

const LoadingModal = ({ loadingModal }) => {
	const localClasses = useLocalStyles()

	if (loadingModal.show) {
		return (
			<div className={localClasses.loadingModal}>
				<Typography variant='h4'>Loading...</Typography>
			</div>
		)
	} else return <></>
}

const mapStateToProps = state => ({
	loadingModal: state.loadingModal,
	darkMode: state.darkMode,
})

const Page = ({ darkMode, loadingModal, width }) => {
	const theme = React.useMemo(
		() =>
			createMuiTheme({
				palette: {
					primary: {
						dark: blue[700],
						light: blue[300],
						main: blue[500],
					},
					type: darkMode ? 'dark' : 'light',
				},
				typography: {
					fontSize: width === 'xs' || width === 'sm' ? 12 : 14,
				},
				spacing: width === 'xs' || width === 'sm' ? 3 : 4,
			}),
		[darkMode, width]
	)

	return (
		<ThemeProvider theme={theme}>
			<CssBaseline />
			<Router>
				<Switch>
					<Route component={HomePage} exact path='/' />
				</Switch>
			</Router>
			<LoadingModal loadingModal={loadingModal} />
		</ThemeProvider>
	)
}

const useLocalStyles = makeStyles(theme => ({
	loadingModal: {
		alignItems: 'center',
		background: theme.palette.background.default,
		display: 'flex',
		height: '100%',
		justifyContent: 'center',
		left: 0,
		position: 'fixed',
		top: 0,
		width: '100%',
		zIndex: 100,
	},
}))

export default withWidth()(connect(mapStateToProps)(Page))
