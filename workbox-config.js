module.exports = {
	globDirectory: 'build/',
	globPatterns: ['**/*.{json,ico,html,js,png,txt,css,woff,woff2}'],
	ignoreURLParametersMatching: [/^utm_/, /^fbclid$/],
	importScripts: ['custom-service-worker.js'],
	swDest: 'build/service-worker.js',
}
