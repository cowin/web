importScripts('https://www.gstatic.com/firebasejs/8.5.0/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/8.5.0/firebase-messaging.js')
importScripts('./localforage.min.js')
localforage.config({ driver: localforage.INDEXEDDB, name: 'cowinjb' })

const API_FIND_BY_DISTRICT =
	'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByDistrict?district_id='
const API_FIND_BY_PINCODE =
	'https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode='

const checkForNewSlots = (oldData, newData) => {
	const oldSessionIDs = oldData.map(session => session.session_id)
	const newSessionIDs = newData.map(session => session.session_id)

	for (let session of newSessionIDs)
		if (!oldSessionIDs.includes(session)) return true

	return false
}

const compileCenters = async (centers, paid = true) => {
	const filters = await localforage.getItem('filtersApplied')
	if (!filters) return

	let oldData = await localforage.getItem('sessions')
	const newData = filterApply(centers, JSON.parse(filters), paid)
	oldData = oldData ? JSON.parse(oldData) : []

	if (checkForNewSlots(oldData, newData)) {
		var options = {
			body: 'Click here to see new slots in your location filtered by your choices.',
			icon: '/logo512.png',
			tag: 'newSession',
		}

		self.registration.showNotification('New Slots', options)
	}

	localforage.setItem('sessions', JSON.stringify(newData))
}

const doubleDigit = num => (num < 10 ? '0' + num : num)

const filterApply = (centers, filters, paid) =>
	centers.reduce((sessions, center) => {
		if (!paid && center.fee_type === 'Paid') return sessions
		const newSessions = center.sessions.filter(item => {
			if (item.available_capacity === 0) return false
			for (const index in filters) {
				if (filters[index] && item[index] !== filters[index]) return false
			}
			return true
		})
		return newSessions.length > 0 ? [...sessions, ...newSessions] : sessions
	}, [])

const getFullDateString = (date = new Date()) =>
	`${doubleDigit(date.getDate())}-${doubleDigit(
		date.getMonth() + 1
	)}-${date.getFullYear()}`

const checkSessions = () => {
	localforage.getItem('inputs').then(value => {
		if (!value) return

		value = JSON.parse(value)
		const day = new Date()

		if (value.district && value.district !== 0)
			fetch(
				`${API_FIND_BY_DISTRICT}${value.district}&date=${getFullDateString(
					day
				)}`
			)
				.then(res => res.json())
				.then(data => compileCenters(data.centers, value.paid))
		else if (value.pincode && value.pincode !== '')
			fetch(
				`${API_FIND_BY_PINCODE}${value.pincode}&date=${getFullDateString(day)}`
			)
				.then(res => res.json())
				.then(data => compileCenters(data.centers, value.paid))
	})
}

self.addEventListener('notificationclick', event => {
	clients.openWindow('https://cowinjb.web.app')
	event.notification.close()
})

const firebaseConfig = {
	apiKey: 'AIzaSyBenxZ9GXopf1pmnTHNGcqPtcAFM3XgP6s',
	authDomain: 'cowinjb.firebaseapp.com',
	projectId: 'cowinjb',
	storageBucket: 'cowinjb.appspot.com',
	messagingSenderId: '233103940496',
	appId: '1:233103940496:web:1585fa6fc01da5b2b67115',
	measurementId: 'G-R7XGL799QF',
}

firebase.initializeApp(firebaseConfig)
const messaging = firebase.messaging()

messaging.onBackgroundMessage(({ data: { title, body } }) => {
	if (title === 'checkSessions') {
		console.log('[firebase-messaging-sw.js]: Message from server', title, body)
		checkSessions()
	} else {
		const notificationOptions = { body, icon: '/logo512.png' }
		self.registration.showNotification(title, notificationOptions)
	}
})
